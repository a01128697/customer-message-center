const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');

var browser;
var page, page1;
var mail, password;

Before(async function() {
    browser = await puppeteer.launch({headless: false});
});

After(async function() {
    //await browser.close();
});

  Given('I open the login page',async function ()  {
    page = await browser.newPage();
    await page.goto('http://localhost:3000/login');
  });
  
  Given('I add mail {string}, password {string}', function (mail1, password1) {
    mail = mail1;
    password = password1;
  });
  
  Given('I enter the mail and password', async function () {
    await page.focus('#email');
    await page.keyboard.type(mail);
    await page.focus('#password');
    await page.keyboard.type(password);
  });
  
  When('I submit the login request', async function () {
    await page.focus('#botonsubmit');
    await page.keyboard.press('Enter', {delay: 1000});
  });

  When('I press the clients button', async function () {
    await page.focus('#clientsbutton');
    await page.keyboard.press('Enter', {delay: 1000});
  });

  When('I press the logout button', async function () {
    await page.focus('#logoutbutton');
    await page.keyboard.press('Enter', {delay: 1000});
  });

  Then('I get redirected to {string}', async function (string) {
    page1 = await browser.newPage(); 
    await page1.goto('http://localhost:3000/login');
  });
  