Feature: Login using an user
  As a user
  So that I can enter the page
  I want to enter using my data

Scenario: login into page and navigation to clients
  Given I open the login page
  And I add mail "alaricoprueba@mail.com", password "patatas"
  And I enter the mail and password
  When I submit the login request
  And I press the clients button
  And I press the logout button
  Then I get redirected to "http://localhost:3000/login"