import chai from 'chai';
import chaiHttp from 'chai-http';
import {expect} from 'chai'
import request from 'supertest'
import 'mocha';

chai.use(chaiHttp);

const url='http://localhost:8080';
var token : string = undefined;

const userCredentials = {
    email:"alaricoprueba@mail.com",
    password : "patatas"
}

const testClient = {
    email : "a01128697@itesm.mx",
    name: "Fernando",
    lastName : "Dávalos",
    street: "Calle Luna",
    exteriorNumber: "43",
    interiorNumber: "13",
    neighborhood: "Jardines de Cuernavaca",
    city: "Cuernavaca",
    state: "Morelos",
    zipCode: 62360,
    birthday: new Date(),
    gender: true,
    phone: 2211739851
}

//var authenticatedUser = request.agent(server);
//Logins every time
before((done) => {
        chai.request(url)
        .post('/api/v0/users/auth/login')
        .send(userCredentials)
        .end((err,response) => {
            expect(response).to.have.status(200);
            token = response.body.token;
            done();
        })
})

//Insert a client without authorization
describe('Insert a client without authentication: ', () => {
    it('should deny insertion',() => {
        chai.request(url)
        .post('/api/v0/clients')
        .send({
            email: 'test@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(401);
        })
    })
})

//Insert a client with authorization
describe('Insert a client: ', () => {
    it('should insert a client',() => {
        chai.request(url)
        .post('/api/v0/clients')
        .set('authorization',token)
        .send({
            email: 'test@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(201);
        })
    })
})

//Insert a client with authorization
describe('Show all data: ', () => {
    it('should show all the clients',() => {
        chai.request(url)
        .get('/api/v0/clients')
        .set('authorization',token)
        .end((err,res) => {
            expect(res).to.have.status(201);
        })
    })
})

//Find a client by mail
describe('Find a client: ', () => {
    it('should find a client',() => {
        chai.request(url)
        .get('/api/v0/clients/:email')
        .set('authorization',token)
        .send({
            email: 'test@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(201);
        })
    })
})

//The user should not be able to get a non existant client
describe('Not finding a client: ', () => {
    it('should not be able to find a client',() => {
        chai.request(url)
        .get('/api/v0/clients/:email')
        .set('authorization',token)
        .send({
            email: 'test3@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(404);
        })
    })
})

//Update a client mail
describe('Update a client: ', () => {
    it('should update a client',() => {
        chai.request(url)
        .post('/api/v0/clients/update/:email')
        .set('authorization',token)
        .send({
            email: 'test@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(201);
        })
    })
})

//Delete a client with authorization
describe('Delete a client: ', () => {
    it('should delete a client',() => {
        chai.request(url)
        .post('/api/v0/clients/delete')
        .set('authorization',token)
        .send({
            email: 'test@company.com'
        })
        .end((err,res) => {
            expect(res).to.have.status(200);
        })
    })
})
