import chai from 'chai';
import chaiHttp from 'chai-http';
import {expect} from 'chai'
import request from 'supertest'
import 'mocha';

chai.use(chaiHttp);

const url='http://localhost:8080';
var token : string = undefined;

const newuser = {
    email: "pepe@mail.com",
    password: "patatas",
    name: "pepe", 
    lastName: "martinez", 
    role: 5
}

const badnewuser = {
    password: "patatas",
    name: "pepe", 
    lastName: "martinez", 
    role: 5
}
const testClient = {
    email : "a01128697@itesm.mx",
    name: "Fernando",
    lastName : "Dávalos",
    street: "Calle Luna",
    exteriorNumber: "43",
    interiorNumber: "13",
    neighborhood: "Jardines de Cuernavaca",
    city: "Cuernavaca",
    state: "Morelos",
    zipCode: 62360,
    birthday: new Date(),
    gender: true,
    phone: 2211739851
}

//Insert a user 
describe('Insert a user: ', () => {
    it('should insert a user',() => {
        chai.request(url)
        .post('/api/v0/users/auth')
        .send(newuser)
        .end((err,res) => {
            expect(res).to.have.status(201);
        })
    })
})

//Fail to insert a user 
describe('Fail to insert a user: ', () => {
    it('should fail to insert a user',() => {
        chai.request(url)
        .post('/api/v0/users/auth')
        .send(badnewuser)
        .end((err,res) => {
            expect(res).to.have.status(500);
        })
    })
})