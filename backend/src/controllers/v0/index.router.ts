import { Router, Request, Response } from 'express';
import { UserRouter } from './users/routes/user.router';
import { ClientRouter} from './clients/routes/client.router';

const router: Router = Router();

router.use('/users', UserRouter);

router.use('/clients', ClientRouter);

router.get('/', async(req: Request, res: Response) => {
    res.send('V0');
})

export const IndexRouter: Router = router;