import { Router, Request, Response } from 'express';
import { requireAuth } from '../../users/routes/auth.router';
import { Client } from './../models/Client';
import * as EmailValidator from 'email-validator';

const router: Router = Router();

//Creates new clients
router.post('/',requireAuth, async (req: Request, res: Response) => {

    const email = req.body.email;
    // check email is valid

    if(!email || !EmailValidator.validate(email)){
        return res.status(400).send({auth: false, message: 'Email is required or malformed'});
    }

    //fin the client
    const client = await Client.findByPk(email);

    if(client){
        return res.status(422).send({auth: false, message: 'Client already exists'})
    }

    

    const newClient = await new Client({
        email : email,
        ...req.body
    })

    let savedClient;
    try{
        savedClient = await newClient.save();
    } catch(e){
        return res.status(400).send('Missing parameter '+ e)
    }

    res.status(201).send({client: savedClient})
})

//get all clients
router.get('/', requireAuth, async (req: Request, res: Response) => {
    const clients = await Client.findAndCountAll({order: [['name', 'DESC']]});

    res.send(clients.rows);
})

//get client by email
router.get('/:email',requireAuth, async(req: Request, res: Response) => {
    let {email} = req.params;
    const client = await Client.findByPk(email);
    if(!client){
        res.status(201).send(client)
    }else{
        res.sendStatus(404);
    }
})


//update client
router.post('/update/:email', requireAuth, async(req: Request, res: Response) => {
    let {email} = req.body;
    console.log(req.params);
    const client = await Client.findByPk(email);
    if(client){
        const hasUpdated = await client.update({
            ...email
        })
        console.log(hasUpdated)
        res.status(201).send(hasUpdated)
    }else{
        res.sendStatus(404)
    }
})


//Delete client
router.post('/delete',requireAuth, async(req: Request, res: Response) => {
    let {email} = req.body;
    console.log(email)
    const client = await Client.findByPk(email);
    if(client){
        const deleteClient = client.destroy();
        res.sendStatus(200);
    }else{
        res.sendStatus(404);
    }
})


export const ClientRouter: Router = router;