import {Table, Column, Model, HasMany, PrimaryKey, CreatedAt, UpdatedAt, AllowNull} from 'sequelize-typescript';
import { Message } from '../../messages/models/Message';

@Table
export class Client extends Model<Client>{

    @PrimaryKey
    @Column
    public email! : string;

    @AllowNull(false)
    @Column
    public name!: string;

    @AllowNull(false)
    @Column
    public lastName!: string;

    @AllowNull(false)
    @Column
    public street!: string;

    @AllowNull(false)
    @Column
    public exteriorNumber!: string;

    @AllowNull(false)
    @Column
    public interiorNumber!: string;

    @AllowNull(false)
    @Column
    public neighborhood!: string;

    @AllowNull(false)
    @Column
    public city!: string;

    @AllowNull(false)
    @Column
    public state!: string;

    @AllowNull(false)
    @Column
    public zipCode!: number;

    @Column
    public birthday : Date;
    
    @AllowNull(false)
    @Column
    public gender!: boolean;

    @AllowNull(false)
    @Column
    public phone!: string;

    @Column
    public whatsapp: string;

    @Column
    public facebook: string;

    @Column
    public twitter: string;

    @Column
    @CreatedAt
    public createdAt: Date = new Date();

    @Column
    @UpdatedAt
    public updatedAt: Date = new Date();

    @HasMany(() => Message)
    messages: Message[]

    short(){
        return{
            email : this.email
        }
    }


}