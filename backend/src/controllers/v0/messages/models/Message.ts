import {Table, Column, Model, AutoIncrement, PrimaryKey, CreatedAt, UpdatedAt, AllowNull, ForeignKey, BelongsTo} from 'sequelize-typescript';
import { Client } from '../../clients/models/Client';
import { User } from '../../users/models/User';

@Table
export class Message extends Model<Message>{

    @PrimaryKey
    @Column
    public id: string;

    @AllowNull(false)
    @Column
    public content: string;

    @ForeignKey(() => Client)
    @Column
    clientEmail: string;

    @BelongsTo(() => Client)
    client: Client;

    @ForeignKey(() => User)
    @Column
    userEmail: string;

    @BelongsTo(() => User)
    user: User;

    @Column
    @CreatedAt
    public createdAt: Date = new Date();

    @Column
    @UpdatedAt
    public updatedAt: Date = new Date();


}