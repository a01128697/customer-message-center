import {Table, Column, Model, HasMany, PrimaryKey, CreatedAt, UpdatedAt, AllowNull} from 'sequelize-typescript';
import { Message } from '../../messages/models/Message';

@Table
export class User extends Model<User>{

    @PrimaryKey
    @Column
    public email: string;

    @AllowNull(false)
    @Column
    public password_hash: string;

    @AllowNull(false)
    @Column
    public name: string;

    @AllowNull(false)
    @Column
    public lastName: string;

    @AllowNull(false)
    @Column
    public role: number;

    @Column
    @CreatedAt
    public createdAt: Date = new Date();

    @Column
    @UpdatedAt
    public updatedAt: Date = new Date();

    @HasMany(() => Message)
    messages: Message[]

    short(){
        return{
            email : this.email
        }
    }


}