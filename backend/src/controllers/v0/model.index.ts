import {User} from './users/models/User';
import {Client} from './clients/models/Client';
import {Message} from './messages/models/Message';

export const V0MODELS = [User,Client,Message];