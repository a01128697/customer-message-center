import {takeLatest, put, all, call} from 'redux-saga/effects';

import UserActionTypes from './user.types';

import XHR from '../../api/xhr';
import { signInFailure, signInSuccess, signOutSuccess, signOutFailure } from './user.actions';

export function* signInWithEmail({payload: {email,password}}){
    try{
        const user = yield XHR.authenticate({email,password});
        yield XHR.setHeader('authorization',user.data.token);
        yield put(signInSuccess(user.data))
    } catch(error){
        yield put(signInFailure(error))
    }
       
}

export function* onEmailSignInStart(){
    yield takeLatest(UserActionTypes.EMAIL_SIGN_IN_START, signInWithEmail)
}


//signout sagas'

export function* signOut(){
    yield put(signOutSuccess());
}

export function* onSignOut(){
    yield takeLatest(UserActionTypes.SIGN_OUT_START, signOut)
}


export function* userSagas(){
    yield all([call(onEmailSignInStart),call(onSignOut)])
}