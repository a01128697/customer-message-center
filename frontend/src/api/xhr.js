import axios from 'axios';


const URLs = {
    'login' : '/users/auth/login',
    'clients' : {
        'get' : '/clients/',
        'create' : '/clients/',
        'delete': '/clients/delete'
    },
    'users' : {
        'get' : '/users/',
        'delete' : '/users/auth/delete',
        'create' : '/users/auth/'
    }
}

class XHR{
    constructor(){
        this.serverInstance = axios.create({
            baseURL: 'http://localhost:8080/api/v0',
            headers: {
                'Content-Type' : 'application/json'
            },
            timeout: 5000
        })
    }

    setHeader = function(headerName, headerValue){
        this.serverInstance.defaults.headers.common[headerName] = headerValue;
    }

    authenticate = function(params){
        console.log(params)
        return this.serverInstance.post(URLs.login,params).catch((e) => console.log(e))
    }

    getClients = function(){
        return this.serverInstance.get(URLs.clients.get).catch((e) => console.log(e));
    }

    getUsers = function(){
        return this.serverInstance.get(URLs.users.get).catch((e) => console.log(e));
    }

    deleteUser = function(params){
        return this.serverInstance.post(URLs.users.delete,params).catch((e) => console.log(e));
    }

    deleteClient = function(params){
        return this.serverInstance.post(URLs.clients.delete,params).catch((e) => console.log(e));
    }

    createUser = function(params){
        return this.serverInstance.post(URLs.users.create,params).catch((e) => console.log(e));
    }

    createClient = function(params){
        return this.serverInstance.post(URLs.clients.create,params).catch((e) => console.log(e));
    }
}

export default new XHR();