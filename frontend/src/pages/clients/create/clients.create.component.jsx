import React from 'react';
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

import FormInput from './../../../components/form-input/form-input.component';
import CustomButton from './../../../components/custom-button/custom-button.component';

import './clients.create.style.scss'
import XHR from './../../../api/xhr';

class CreateClient extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email : '',
            name: '',
            lastName : '',
            street: '',
            exteriorNumber : '',
            interiorNumber : '',
            neighborhood : '',
            city : '',
            state : '',
            zipCode : '',
            birthday: '',
            gender: false,
            phone: '',
            facebook:'',
            twitter: '',
            whatsapp: ''
        }
    }

    handleSubmit = async event => {
        event.preventDefault();
        const {email, name, street, exteriorNumber, lastName, interiorNumber, neighborhood, city, state, zipCode, birthday, gender, phone, facebook,twitter, whatsapp } = this.state;
        const { history, match} = this.props;
        //console.log(email, name, street, exteriorNumber, lastName, interiorNumber, neighborhood, city, state, zipCode, birthday, gender, phone, facebook,twitter, whatsapp)
        XHR.createClient({email, name, street, exteriorNumber, lastName, interiorNumber, neighborhood, city, state, zipCode, birthday, gender, phone, facebook,twitter, whatsapp}).then((data) => {
            history.push(`/clients`)
        })
    }

    handleChange = event => {
        const {name,value} = event.target;

        this.setState({[name] : value})
    }


    render(){
        const {email, name, street, exteriorNumber, lastName, interiorNumber, neighborhood, city, state, zipCode, birthday, gender, phone, facebook,twitter, whatsapp} = this.state;
        return(
            <div className="sign-up">
                <h2 className="title">Create Client</h2>
                <form className="sign-up-form" onSubmit={this.handleSubmit}>
                    <FormInput
                        type="text"
                        name='name'
                        value={name}
                        onChange={this.handleChange}
                        label='Name'
                        required
                    />
                    <FormInput
                        type="text"
                        name='lastName'
                        value={lastName}
                        onChange={this.handleChange}
                        label='Last Name'
                        required
                    />
                    <FormInput
                        type="email"
                        name='email'
                        value={email}
                        onChange={this.handleChange}
                        label='Email'
                        required
                    />
                    <FormInput
                        type="text"
                        name='street'
                        value={street}
                        onChange={this.handleChange}
                        label='Street'
                        required
                    />
                    <FormInput
                        type="text"
                        name='exteriorNumber'
                        value={exteriorNumber}
                        onChange={this.handleChange}
                        label='Exterior Number'
                        required
                    />
                    <FormInput
                        type="text"
                        name='interiorNumber'
                        value={interiorNumber}
                        onChange={this.handleChange}
                        label='Interior Number'
                        required
                    />
                    <FormInput
                        type="text"
                        name='neighborhood'
                        value={neighborhood}
                        onChange={this.handleChange}
                        label='Neighborhood'
                        required
                    />
                    <FormInput
                        type="text"
                        name='city'
                        value={city}
                        onChange={this.handleChange}
                        label='City'
                        required
                    />
                    <FormInput
                        type="text"
                        name='state'
                        value={state}
                        onChange={this.handleChange}
                        label='State'
                        required
                    />
                    <FormInput
                        type="text"
                        name='zipCode'
                        value={zipCode}
                        onChange={this.handleChange}
                        label='Zip Code'
                        required
                    />
                    <FormInput
                        type="date"
                        name='birthday'
                        value={birthday}
                        onChange={this.handleChange}
                        label='Birthday'
                        required
                    />
                    <FormInput
                        type="checkbox"
                        name='gender'
                        value={gender}
                        onChange={this.handleChange}
                        label='Gender'
                        required
                    />
                    <FormInput
                        type="text"
                        name='phone'
                        value={phone}
                        onChange={this.handleChange}
                        label='Phone'
                        required
                    />
                    <FormInput
                        type="text"
                        name='whatsapp'
                        value={whatsapp}
                        onChange={this.handleChange}
                        label='Whatsapp'
                        required
                    />
                    <FormInput
                        type="text"
                        name='twitter'
                        value={twitter}
                        onChange={this.handleChange}
                        label='Twitter'
                        required
                    />
                    <FormInput
                        type="text"
                        name='facebook'
                        value={facebook}
                        onChange={this.handleChange}
                        label='Facebook'
                        required
                    />
                    <CustomButton type="submit">CREATE USE</CustomButton>
                </form>
            </div>
        )
    }
}



export default withRouter(CreateClient);