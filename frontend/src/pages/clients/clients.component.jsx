import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'
import {withRouter} from 'react-router-dom'
import XHR from './../../api/xhr';

import {selectCurrentUser} from './../../redux/user/user.selectors';


import './clients.style.scss';
import CustomTable from "../../components/custom-table/custom-table.component";
import CustomTableRow from "../../components/custom-table-row/custom-table-row.component";
import CustomTableCell from "../../components/custom-table-cell/custom-table-cell.component";
import CustomButton from "./../../components/custom-button/custom-button.component";

class ClientsPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            clients : {}
        };
    }

    deleteUser = email => {
        var self = this;
        XHR.deleteClient({email}).then(response => {
            XHR.getClients().then((response) => {
                self.setState({clients : response.data})
            })
        })
    }

    componentDidMount(){
        var self = this;
        XHR.getClients().then((response) => {
            self.setState({clients : response.data})
        })
    }

    render(){
        const { clients } = this.state;
        const { currentUser,history, match } = this.props;
        return(
            <div>
                Clients page
                {  
                    currentUser.user === 5 ? <CustomButton type="button" onClick={() => history.push(`${match.url}/create`)}  >CREATE CLIENT</CustomButton> : null
                }
                <CustomTable>
                    <CustomTableRow>
                        <CustomTableCell>Email</CustomTableCell>
                        <CustomTableCell>Name</CustomTableCell>
                        <CustomTableCell>Street</CustomTableCell>
                        <CustomTableCell>Exterior Number</CustomTableCell>
                        <CustomTableCell>Interior Number</CustomTableCell>
                        <CustomTableCell>Neighborbood</CustomTableCell>
                        <CustomTableCell>City</CustomTableCell>
                        <CustomTableCell>State</CustomTableCell>
                        <CustomTableCell>Zip Code</CustomTableCell>
                        <CustomTableCell>Birthday</CustomTableCell>
                        <CustomTableCell>Gender</CustomTableCell>
                        <CustomTableCell>Phone</CustomTableCell>
                        <CustomTableCell>Whatsapp</CustomTableCell>
                        <CustomTableCell>Facebook</CustomTableCell>
                        <CustomTableCell>Twitter</CustomTableCell>
                        {  
                            currentUser.user === 5 ? <CustomTableCell>Actions</CustomTableCell> : null
                        }
                    </CustomTableRow>
                {
                    clients.length ? (
                        clients.map((client) => (
                            <CustomTableRow key={client.email}>
                                <CustomTableCell>{client.email}</CustomTableCell>
                                <CustomTableCell>{client.name}</CustomTableCell>
                                <CustomTableCell>{client.street}</CustomTableCell>
                                <CustomTableCell>{client.exteriorNumber}</CustomTableCell>
                                <CustomTableCell>{client.interiorNumber}</CustomTableCell>
                                <CustomTableCell>{client.neighborhood}</CustomTableCell>
                                <CustomTableCell>{client.city}</CustomTableCell>
                                <CustomTableCell>{client.state}</CustomTableCell>
                                <CustomTableCell>{client.zipCode}</CustomTableCell>
                                <CustomTableCell>{client.birthday}</CustomTableCell>
                                <CustomTableCell>{client.gender}</CustomTableCell>
                                <CustomTableCell>{client.phone}</CustomTableCell>
                                <CustomTableCell>{client.whatsapp}</CustomTableCell>
                                <CustomTableCell>{client.facebook}</CustomTableCell>
                                <CustomTableCell>{client.twitter}</CustomTableCell>
                                {
                                    currentUser.user === 5 ? 
                                    <CustomTableCell>
                                        <CustomButton type="button" onClick={() => this.deleteUser(client.email)}>Delete</CustomButton>
                                    </CustomTableCell> : null
                                }
                            </CustomTableRow>
                        ))
                    ) : null
                }
                </CustomTable>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    currentUser : selectCurrentUser
  })

export default connect(mapStateToProps)(withRouter(ClientsPage));