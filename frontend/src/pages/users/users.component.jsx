import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'
import {withRouter} from 'react-router-dom'
import XHR from './../../api/xhr';

import {selectCurrentUser} from './../../redux/user/user.selectors';

import './users.style.scss';
import CustomTable from "../../components/custom-table/custom-table.component";
import CustomTableRow from "../../components/custom-table-row/custom-table-row.component";
import CustomTableCell from "../../components/custom-table-cell/custom-table-cell.component";
import CustomButton from "./../../components/custom-button/custom-button.component";

class UsersPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            users : {}
        };
    }

    deleteUser = email => {
        var self = this;
        XHR.deleteUser({email}).then(response => {
            XHR.getUsers().then((response) => {
                self.setState({users : response.data})
            })
        })
    }

    componentDidMount(){
        var self = this;
        XHR.getUsers().then((response) => {
            self.setState({users : response.data})
        })
    }

    render(){
        const { users } = this.state;
        const { currentUser,history, match } = this.props;
        return(
            <div>
                UsersPage page
                {  
                    currentUser.user === 5 ? <CustomButton type="button" onClick={() => history.push(`${match.url}/create`)}  >CREATE USER</CustomButton> : null
                }
                <CustomTable>
                    <CustomTableRow>
                        <CustomTableCell>Email</CustomTableCell>
                        <CustomTableCell>Name</CustomTableCell>
                        <CustomTableCell>Last Name</CustomTableCell>
                        <CustomTableCell>Role</CustomTableCell>
                        {  
                            currentUser.user === 5 ? <CustomTableCell>Actions</CustomTableCell> : null
                        }
                    </CustomTableRow>
                {
                    users.length ? (
                        users.map((user) => (
                            <CustomTableRow key={user.email}>
                                <CustomTableCell>{user.email}</CustomTableCell>
                                <CustomTableCell>{user.name}</CustomTableCell>
                                <CustomTableCell>{user.lastName}</CustomTableCell>
                                <CustomTableCell>{user.role === 5 ? 'ADMIN' : 'CAPTURIST'}</CustomTableCell>
                                {
                                    currentUser.user === 5 ? 
                                    <CustomTableCell>
                                        <CustomButton type="button" onClick={() => this.deleteUser(user.email)}>Delete</CustomButton>
                                    </CustomTableCell> : null
                                }
                            </CustomTableRow>
                        ))
                    ) : null
                }
                </CustomTable>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    currentUser : selectCurrentUser
  })

export default connect(mapStateToProps)(withRouter(UsersPage));