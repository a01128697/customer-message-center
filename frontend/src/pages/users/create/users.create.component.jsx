import React from 'react';
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

import FormInput from './../../../components/form-input/form-input.component';
import CustomButton from './../../../components/custom-button/custom-button.component';

import './users.create.style.scss'
import XHR from './../../../api/xhr';

class CreateUser extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email : '',
            password : '',
            confirmPassword : '',
            name: '',
            lastName : '',
            role : 5
        }
    }

    handleSubmit = async event => {
        event.preventDefault();
        const {email, name, password, confirmPassword, lastName, role} = this.state;
        const { history, match} = this.props;
        if(password !== confirmPassword){
            alert("Passwords don't match")
            return;
        }
        console.log(email, name, password, confirmPassword, lastName, role)
        XHR.createUser({email,name,password,lastName,role}).then((data) => {
            history.push('/users')
        })
    }

    handleChange = event => {
        const {name,value} = event.target;

        this.setState({[name] : value})
    }


    render(){
        const {email, name, password, confirmPassword, lastName, role} = this.state;
        return(
            <div className="sign-up">
                <h2 className="title">Create User</h2>
                <form className="sign-up-form" onSubmit={this.handleSubmit}>
                    <FormInput
                        type="text"
                        name='name'
                        value={name}
                        onChange={this.handleChange}
                        label='Name'
                        required
                    />
                    <FormInput
                        type="text"
                        name='lastName'
                        value={lastName}
                        onChange={this.handleChange}
                        label='Last Name'
                        required
                    />
                    <FormInput
                        type="email"
                        name='email'
                        value={email}
                        onChange={this.handleChange}
                        label='Email'
                        required
                    />
                    <FormInput
                        type="password"
                        name='password'
                        value={password}
                        onChange={this.handleChange}
                        label='Password'
                        required
                    />
                    <FormInput
                        type="password"
                        name='confirmPassword'
                        value={confirmPassword}
                        onChange={this.handleChange}
                        label='Confirm Password'
                        required
                    />
                    <FormInput
                        type="number"
                        name='role'
                        value={role}
                        onChange={this.handleChange}
                        label='Role'
                        required
                    />
                    <CustomButton type="submit">CREATE USE</CustomButton>
                </form>
            </div>
        )
    }
}



export default withRouter(CreateUser);