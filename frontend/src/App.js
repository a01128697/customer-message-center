import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux'
import { createStructuredSelector } from 'reselect'

import './App.css';

import Header from './components/header/header.component';
import LoginPage from './pages/login/login.component';
import ClientsPage from './pages/clients/clients.component';
import UsersPage from './pages/users/users.component';
import CreateUser from './pages/users/create/users.create.component'
import CreateClient from './pages/clients/create/clients.create.component';

import { selectCurrentUser } from './redux/user/user.selectors';


const App = ({currentUser}) => {
  return (
    <div>
      <Header/>
      <Switch>
        <Route exact path="/login" render={() => currentUser ? (<Redirect to="/users"/>) : (<LoginPage/>)}/>
        <Route exact path="/clients" render={() => !currentUser ? (<Redirect to="/login"/>) : (<ClientsPage/>)}/>
        <Route exact path="/users" render={() => !currentUser ? (<Redirect to="/login"/>) : (<UsersPage/>)}/>
        <Route exact path="/users/create" render={() => !currentUser ? (<Redirect to="/login"/>) : (<CreateUser/>)}/>
        <Route exact path="/clients/create" render={() => !currentUser ? (<Redirect to="/login"/>) : (<CreateClient/>)}/>
      </Switch>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  currentUser : selectCurrentUser
})

export default connect(mapStateToProps)(App);
