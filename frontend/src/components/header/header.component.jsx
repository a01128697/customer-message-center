import React from "react";
import { Link } from "react-router-dom";
import {connect} from 'react-redux'
import { createStructuredSelector } from 'reselect';


import { selectCurrentUser} from '../../redux/user/user.selectors'
import { signOutStart } from '../../redux/user/user.actions'

import "./header.style.scss";

const Header = ({ currentUser, signOutStart }) => (
  <div className="header">
    <Link to="/" className="logo-container">
    </Link>
    <div className="options">
      <Link id= "clientsbutton"className="option" to="/clients">
        CLIENTS
      </Link>
      <Link className="option" to="/users">
        USERS
      </Link>
      {currentUser ? (
        <div id= "logoutbutton" className="option" onClick={signOutStart}>
          SIGN OUT
        </div>
      ) : (
        <Link className="option" to="/login">
          SIGN IN
        </Link>
      )}
    </div>
  </div>
);

const mapStateToProps = createStructuredSelector({
  currentUser : selectCurrentUser
})

const mapDispatchToProps = dispatch => ({
  signOutStart : () => dispatch(signOutStart())
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
