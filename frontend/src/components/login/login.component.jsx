import React from "react";
import { connect } from 'react-redux'

import { emailSignInStart } from './../../redux/user/user.actions';


import FormInput from "../form-input/form-input.component";
import CustomButton from "../custom-button/custom-button.component";


import "./login.style.scss";

class Login extends React.Component {
  constructor() {
    super();

    this.state = {
      email: "",
      password: ""
    };
  }

  handleSubmit = event => {
      event.preventDefault();
      const {emailSignInStart} = this.props;
      const {email, password} = this.state;
      emailSignInStart(email,password)
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };



  render() {
    const { email, password } = this.state;
    return (
      <div className="login">
        <div className="login-form">
          <div className="login-form__content">
            <h2 className="login-form__content__title">Login</h2>
            <form className="login-form__inputs" onSubmit={this.handleSubmit}>
              <FormInput
                type="text"
                name="email"
                label="Email"
                id="email"
                value={email}
                handleChange={this.handleChange}
                required
              />
              <FormInput
                type="password"
                name="password"
                label="Password"
                id = "password"
                value={password}
                handleChange={this.handleChange}
                required
              />
              <CustomButton id = "botonsubmit"name="botonsubmit" type="submit">Enter</CustomButton>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  emailSignInStart: (email, password) => dispatch(emailSignInStart({email, password}))
})

export default connect(null,mapDispatchToProps)(Login);
